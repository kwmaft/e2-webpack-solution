const
    gulp = require("gulp"),
    named = require("vinyl-named"),
    webpack = require("webpack"),
    webpackStream = require("webpack-stream");

gulp.task("default", function () {
    return gulp
        .src(["src/webpack/app.js"])
        .pipe(named())
        .pipe(webpackStream(require("./webpack.config.js"), webpack))
        .pipe(gulp.dest("dist/"));
});