const
    path = require("path"),
    MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    /**
     * Webpack needs a mode to run in. This is usually set via the NODE_ENV variable.
     * For convenience reasons we just set it to development here. When setting the mode
     * to production, webpack will automatically minify and uglify Javascript code.
     */
    mode: "development",

    /**
     * Enable source maps. The code produced by webpack and babel is a little bit cryptic
     * sometimes. With source maps on the devtools of a browser show the actual code when
     * debugging.
     */
    devtool: "source-map",

    /**
     * Watch for file changed and rebundle each time a file changes.
     */
    watch: true,

    /**
     * Disable unnecessary warnings produced by using gulp-webpack
     */
    stats: {
        entrypoints: false
    },

    // If webpack cli is used instead of the gulp, the entry points have to be
    // configured here, otherwise they are configured with gulp.
    // entry: {
    //     app: "src/app.js"
    // },

    output: {
        // Same for the output path, also configured via gulp.
        //path: "dist/",
        filename: "js/[name].js"
    },

    /**
     * Here absolute module paths can be provided. This enables easier
     * module loading relative to these paths, instead of relative to the current
     * working file.
     */
    resolve: {
        modules: [
            path.resolve(__dirname, "src", "webpack"),
            path.resolve(__dirname, "node_modules")
        ]
    },

    module: {
        rules: [{
            // babel-loader compiles ES6 to ES5 for browser compatibility
            test: /\.js$/,
            exclude: /(node_modules|bower_components|vendor)/,
            loader: "babel-loader",
        }, {
            // css-loader enables css dependencies directly from a js module
            test: /\.css$/,
            exclude: /(node_modules|bower_components|vendor)/,
            use: [MiniCssExtractPlugin.loader, "css-loader"]
        }, {
            // handlebars-loader is able to load handlebars templates and converts
            // them directly into a template function
            test: /\.hbs$/,
            exclude: /(node_modules|bower_components|vendor)/,
            loader: "handlebars-loader",
            query: {
                partialDirs: [path.resolve(__dirname, "src/webpack/templates/partials")]
            }
        }]
    },

    plugins: [
        // extract css to seperate files, otherwise the styles are stored directly
        // in the javascript files, which is quite nice, but cumbersome for huge js and css files
        // in the web a file shouldn't be bigger than 400kb --> chunk it!
        new MiniCssExtractPlugin({
            filename: "css/[name].css"
        })
    ],

    /**
     * enable the vendor chunk. Every module which is loaded from node_modules is place
     * inside that chunk, creating a vendor.js with e.g. jQuery, Bootstrap.js, ...
     */
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    chunks: 'initial',
                    name: 'vendor',
                    enforce: true
                },
            }
        }
    }
};