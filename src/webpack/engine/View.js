import $ from "jquery";

/**
 * @class View
 */
export default class View {
    constructor(template) {
        /**
         * The handlebars template for rendering the content.
         */
        this.template = template;

        /**
         * Element in which the content will be rendered.
         *
         * @type {jQuery}
         */
        this.$el = $("<div/>");

        /**
         * View was already attached to the DOM via render.
         * @type {boolean}
         */
        this.attached = false;

        /**
         * View has already been rendered. Changed data will rerender.
         * @type {boolean}
         */
        this.rendered = false;
    }

    /**
     * Call this when the data has changed to trigger a rerender.
     */
    dataChanged() {
        if (!this.rendered) {
            return;
        }

        this.render();
    }

    /**
     * Set the container to which the views element will be attached
     *
     * @param {jQuery} $container
     */
    renderTo($container) {
        this.$container = $container;

        this.render();
    }

    /**
     * When extending a view, return the view model form this method.
     *
     * @returns {object}
     */
    getModel() {
        return {};
    }

    /**
     * Render the view, attach it to the DOM and render the template.
     */
    render() {
        if (!this.$container) {
            return;
        }

        if (!this.attached) {
            this.$container.append(this.$el);
            this.attached = true;

            this.onAttach();
        }

        this.$el.empty().html(this.template({model: this.getModel()}));
        this.rendered = true;

        this.onRender();
    }

    /**
     * Will be called after the view is attached to the body.
     */
    onAttach() {
    }

    /**
     * Will be called after each render cycle.
     */
    onRender() {
    }
}