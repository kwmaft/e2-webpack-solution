import View from "engine/View";

//require("../styles/user.css");

export default class UsersView extends View {
    constructor() {
        super(require("../templates/users.hbs"));

        this.users = [];
    }

    getModel() {
        return this.users;
    }

    onAttach() {
        /**
         * Once the element is attached to the DOM we can bind
         * listeners on it. The content is rendered later on so
         * we can't bind them directly to the button but have to listen
         * to the bubbled event on th element itself.
         */
        this.$el.on("click", ".js-add-user", () => {
            this.addUser({name: "New User"});
        });
    }

    addUser(user) {
        this.users.push(user);
        this.dataChanged();
    }
}