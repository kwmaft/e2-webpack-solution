require("./styles/app.css");

import $ from "jquery";
import UsersView from "views/UsersView";

$(function () {
    const usersView = new UsersView();

    usersView.addUser({
        name: "David Riedl"
    });

    usersView.addUser({
        name: "Michael Kuss"
    });

    usersView.addUser({
        name: "Mc Hammer"
    });

    usersView.renderTo($(".js-users"));

    usersView.addUser({name: "Foo Bar"});
});